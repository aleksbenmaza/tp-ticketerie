import { Module } from '@nestjs/common';
import { TicketController } from './ticket/ticket.controller';
import { TicketService } from './ticket/ticket.service';
//import { APP_GUARD } from "@nestjs/core";
//import { ApiUsageGuard } from "./api-usage-guard.service";
import { TokenController } from './token/token.controller';
import { UserService } from './user/user.service';
import {JwtModule} from "@nestjs/jwt";

import * as jwtProperties from '../conf/jwt-properties.json';
import {JWTManager} from "./helper/jwt-manager.service";

@Module({
  imports: [
      JwtModule.register({
          secretOrPrivateKey: (jwtProperties as any).jwtSecret,
          signOptions: {
              expiresIn: (jwtProperties as any).jwtLifetime,
          }
      }),
  ],
  controllers: [
      TicketController,
      TokenController,
  ],
  providers: [
      TicketService,
      UserService,
      JWTManager,
      /*{
          provide: APP_GUARD,
          useClass: ApiUsageGuard,
      },*/
  ],
})
export class AppModule {}
