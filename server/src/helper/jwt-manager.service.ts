import {Injectable} from "@nestjs/common";
import {JwtService} from "@nestjs/jwt";
import * as jwtProperties from '../../conf/jwt-properties.json';

const BEARER_PREFIX: string = 'Bearer ';

@Injectable()
export class JWTManager {

    constructor(private readonly jwtService: JwtService) {}

    encode(userId: number, roles: string[]): string {
        return BEARER_PREFIX + this.jwtService.sign({userId: userId, roles: roles});
    }

    decode(token: string): {userId: number, roles: string[]} {
        return this.jwtService.decode(
            token.replace(BEARER_PREFIX, ''),
            {
                complete: true}
                )['payload'] as {userId: number, roles: string[]};
    }

    get expirationTime(): number {
        return Date.now() + (jwtProperties as any).jwtLifetime * 1000;
    }
}