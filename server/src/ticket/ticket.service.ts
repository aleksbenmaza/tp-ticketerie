import { Injectable } from '@nestjs/common';
import {Ticket} from "../../../common/src/model/ticket.model";
import {TicketService as TicketServiceInterface} from "../../../common/src/service/ticket.service";
import * as mockData from "../../conf/mock-data.json";

const data = mockData as any;

@Injectable()
export class TicketService implements TicketServiceInterface {

    private sequence: number;

    private tickets: Ticket[];

    constructor() {
        this.tickets = data.tickets;
        this.sequence = this.tickets.sort(
            (ticket0, ticket1) => ticket0.id - ticket1.id
        )[this.tickets.length - 1].id + 1;
    }

    getAll(): Ticket[] {
        return [].concat(...this.tickets);
    }

    getOne(id: number): Ticket {
        return this.tickets.find(ticket => ticket.id == id);
    }

    add(ticket: Ticket): Ticket {
        this.tickets.push(ticket);
        ticket.id = ++this.sequence;
        return ticket;
    }

    set(id: number, newTicket: Ticket): void {
        let oldTicket: Ticket;
        oldTicket = this.tickets.find(ticket => ticket.id == id);
        oldTicket.description = newTicket.description;
        oldTicket.expiration = newTicket.expiration;
        oldTicket.name = newTicket.name;
        oldTicket.nbLefts = newTicket.nbLefts;
        oldTicket.urlImage = newTicket.urlImage;
    }

    remove(id: number): void {
        this.tickets  = this.tickets.slice(
            this.tickets.indexOf(
                this.tickets.find(ticket => ticket.id == id)
            ),
            1
        );
    }

    contains(id: number): boolean {
        return this.tickets.find(ticket => ticket.id == id) != null;
    }
}
