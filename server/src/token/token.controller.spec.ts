import { Test, TestingModule } from '@nestjs/testing';
import { TokenController } from './token.controller';

describe('Token Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [TokenController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: TokenController = module.get<TokenController>(TokenController);
    expect(controller).toBeDefined();
  });
});
