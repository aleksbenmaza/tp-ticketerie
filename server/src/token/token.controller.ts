import {BadRequestException, Body, Controller, Post} from '@nestjs/common';
import {create, Response} from "../../../common/src/response";
import {Token} from "../../../common/src/model/token.model";
import {TokenCreation} from "../../../common/src/model/token-creation.model";
import {UserService} from "../user/user.service";
import {JWTManager} from "../helper/jwt-manager.service";
import {User} from "../../../common/src/model/user.model";
import {hashSync} from "bcrypt";

@Controller('tokens')
export class TokenController {

    constructor(
        private readonly userService: UserService,
        private readonly jwtManager: JWTManager
    ) {}

    @Post()
    onPost(@Body() tokenCreation: TokenCreation): Response<Token> {
        let user: User;
        let str: string = hashSync(
            tokenCreation.emailAddress + tokenCreation.password,
            '$2a$10$7h/0SQ4FXRG5eX3602o3/.aO.RYkxKuhGkzvIXHLUiMJlFt1P.6Pe'
        );

        user = this.userService.getOneByEmailAddressAndHash(tokenCreation.emailAddress, str);

        if(!user)
            throw new BadRequestException;

        return create(true, {
                user: {...user, hash: undefined},
                expirationTime: this.jwtManager.expirationTime,
                value: this.jwtManager.encode(user.id, user.roles)
        });
    }
}
