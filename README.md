LICENSE
-------

Please, consider reading the [license agreement](LICENSE).

REQUIREMENTS
------------

- npm

To build both the client & server side projects, you will need [npm](https://www.npmjs.com/get-npm), run `npm install` then `npm start` on each root (`/client` & `/server`), you will want to open your favourite web browser on `localhost:5400`.

_P.S._
------

- Use `admin@admin.admin` email address and `admin` in order to log in.

- There is some trouble with the logout button, you might need to click more than once for it to work.