import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './components/app.component';
import {TicketListComponent} from './components/main/ticket-list/ticket-list.component';
import {TicketService} from "./services/ticket.service";
import {TicketDetailsComponent} from './components/main/ticket-list/ticket-details/ticket-details.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {LoadingWheelComponent} from "./components/common/loading-wheel/loading-wheel.component";
import {ConfirmationModalComponent} from './components/common/confirmation-modal/confirmation-modal.component';
import {TicketCreationFormComponent} from "./components/main/ticket-creation-form/ticket-creation-form.component";
import {FormsModule} from "@angular/forms";
import { LoginFormComponent } from './components/login-form/login-form.component';
import {UserService} from "./services/user.service";
import {AuthInterceptor} from "./auth-interceptor";
import { NavbarComponent } from './components/main/navbar/navbar.component';
import {RouterModule, Routes} from "@angular/router";
import { MainComponent } from './components/main/main.component';
import { RoutedClassDirective } from './directives/routed-class.directive';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'ticket/:id',
    component: MainComponent,
  },
  {
    path: 'ticket/creer',
    component: MainComponent,
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TicketListComponent,
    TicketDetailsComponent,
    LoadingWheelComponent,
    ConfirmationModalComponent,
    TicketCreationFormComponent,
    LoginFormComponent,
    NavbarComponent,
    MainComponent,
    RoutedClassDirective,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    TicketService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
