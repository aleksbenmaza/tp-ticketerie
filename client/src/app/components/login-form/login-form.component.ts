import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TokenCreation} from "../../../../../common/src/model/token-creation.model";
import {UserService} from "../../services/user.service";
import {Token} from "../../../../../common/src/model/token.model";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  tokenCreation: TokenCreation;

  persistentToken: boolean;

  @Output("tokenCreated")
  eventEmitter: EventEmitter<Token>;

  constructor(private readonly userService: UserService) {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
    this.tokenCreation = this.emptyTokenCreation;
  }

  get completed(): boolean {
    let isValid: (subject: string) => boolean;

    isValid = subject => subject != null && subject.trim().length != 0;

    return isValid(this.tokenCreation.emailAddress) &&
           isValid(this.tokenCreation.password);
  }

  createToken(): void {
    this.userService.addToken(this.tokenCreation).then(
      tokenResponse => {
        tokenResponse.response.persistent = this.persistentToken;
        this.eventEmitter.emit(tokenResponse.response);
        this.emptyTokenCreation
      }
    );
  }

  private get emptyTokenCreation(): TokenCreation {
    return {
      emailAddress: null,
      password: null
    }
  }
}
