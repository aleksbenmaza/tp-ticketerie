import {Component, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {

  @Input()
  title: string;

  @Input()
  text: string;

  @Output('confirm')
  eventEmitter: EventEmitter<boolean>;

  constructor() {
    this.eventEmitter = new EventEmitter;
  }

  ngOnInit() {
  }

  set confirmation(value: boolean) {
    this.eventEmitter.emit(value);
    jQuery("#confirmationModal").modal('toggle');
  }
}
