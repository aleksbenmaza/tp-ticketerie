import { Component, OnInit } from '@angular/core';
import {Ticket} from "../../../../../../common/src/model/ticket.model";
import {TicketService} from "../../../services/ticket.service";
import {environment} from "../../../../environments/environment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ticket-creation-form',
  templateUrl: './ticket-creation-form.component.html',
  styleUrls: ['./ticket-creation-form.component.css']
})
export class TicketCreationFormComponent implements OnInit {

  ticket: Ticket;

  constructor(
    private readonly ticketService: TicketService,
    private readonly router: Router
  ) {
    this.ticket = this.emptyTicket;
  }

  ngOnInit() {
  }

  createTicket(): void {
    this.ticketService.add(this.ticket)
                      .then(
                        ticketResponse => {
                          this.ticketService
                            .ticketCreated
                            .emit(ticketResponse.response);
                          this.router.navigateByUrl(
                            `/ticket/${ticketResponse.response.id}`
                          )
                        }
                      );
    this.ticket = this.emptyTicket;
  }

  get urlPattern(): string {
    return environment.urlPattern;
  }

  private get emptyTicket(): Ticket {
    return {
      name: null,
      expiration: null,
      description: null,
      nbLefts: null,
      urlImage: null,
    }
  }
}
