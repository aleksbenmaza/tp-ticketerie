import { Component, OnInit } from '@angular/core';
import {Ticket} from "../../../../../../common/src/model/ticket.model";
import {TicketService} from "../../../services/ticket.service";
import {Location} from "@angular/common";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {

  tickets: Ticket[];

  selectedTicketId: number;

  constructor(private readonly ticketService: TicketService, private readonly location: Location) {
    this.ticketService
        .ticketCreated
        .subscribe(
          ticket => this.tickets.push(ticket)
        )
  }

  ngOnInit() {
    this.ticketService.getAll()
                      .then(response => this.tickets = response.response)
  }

  deleteTicket(confirmation: boolean): void {
    if(confirmation)
      this.ticketService.remove(this.selectedTicketId)
                        .then(
                          _ => {
                            this.tickets = this.tickets.filter(
                              ticket => ticket.id != this.selectedTicketId
                            );
                            this.selectedTicketId = null;
                            location.go('/')
                          }
                   );
    else
      this.selectedTicketId = null
  }

  get selectedTicket(): Ticket {
    return (this.tickets || []).find(ticket => ticket.id == this.selectedTicketId)
  }
}
