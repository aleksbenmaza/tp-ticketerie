import { Component } from '@angular/core';
import {UserService} from "../services/user.service";
import {Token} from "../../../../common/src/model/token.model";
import {User} from "../../../../common/src/model/user.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';

  constructor(private readonly userService: UserService) {}

  tokenCreated(token: Token): void {
    this.userService.token = token;
    setTimeout(() => this.userService.token = null, token.expirationTime - Date.now())
  }

  get authenticated(): boolean {
    return this.userService.token != null;
  }

  get user(): User {
    return this.authenticated ?
      this.userService.token.user :
      null;
  }
}
