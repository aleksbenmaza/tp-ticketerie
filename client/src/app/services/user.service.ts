import { Injectable } from '@angular/core';
import {Service} from "./service";
import {TokenCreation} from "../../../../common/src/model/token-creation.model";
import {Token} from "../../../../common/src/model/token.model";
import {Response} from "../../../../common/src/response";
import {LocalStorage} from "ngx-store";

const TOKEN_ENDPOINT = 'tokens';

@Injectable()
export class UserService extends Service {

  private nonPersistentToken: Token;

  @LocalStorage()
  private persistentToken: Token;

  addToken(tokenCreation: TokenCreation): Promise<Response<Token>> {
    return this.httpClient.post<Response<Token>>(
      `http://${this.apiServer}/${TOKEN_ENDPOINT}`,
      tokenCreation
    ).toPromise();
  }

  get token(): Token {
    return this.nonPersistentToken || this.persistentToken;
  }

  set token(value: Token) {
    this.persistentToken = this.nonPersistentToken
                         = null;

    if(value != null)
      if(value.persistent)
        this.persistentToken = value;
      else
        this.nonPersistentToken = value;
  }
}
