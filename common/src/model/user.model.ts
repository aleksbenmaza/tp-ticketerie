
export interface User {

    id: number
    emailAddress: string
    hash?: string
    firstName: string
    lastName: string
    roles: string[]
}