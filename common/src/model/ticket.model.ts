
export interface Ticket {
    id?: number
    name: string
    description: string
    expiration: Date
    nbLefts: number
    urlImage: string
}