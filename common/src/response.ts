export interface Response<T> {
    success: boolean
    response?: T
    error?: string
}

export function create<T>(success: boolean, response?: T, error?: string): Response<T> {
    return {
      success: success,
      response: response,
      error: error,
    };
}