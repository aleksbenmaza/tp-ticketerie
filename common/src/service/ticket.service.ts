import {Ticket} from "../model/ticket.model";
import {Response} from "../response";

export interface TicketService {
    getAll(): Ticket[] | Promise<Response<Ticket[]>>;

    getOne(id: number): Ticket | Promise<Response<Ticket>>;

    add(ticket: Ticket): Ticket | Promise<Response<Ticket>>;

    set(id: number, newTicket: Ticket): void | Promise<Response<void>>;

    remove(id: number): void | Promise<Response<void>>;
}